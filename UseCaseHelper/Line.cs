﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseHelper
{
    class Line
    {
        private Point lineStart;
        private Point lineEnd;
        public bool selected;

        private Actor actor;
        private UseCase useCase;

        public Line(Actor actor, UseCase useCase)
        {
            this.actor = actor;
            this.useCase = useCase;
            refreshConnectingPoints();
            selected = false;
        }

        public void refreshConnectingPoints()
        {
            Point actorPoint = actor.connectingPointLeft;
            Point useCasePoint = useCase.connectingPointLeft;

            if (actorPoint.X < useCasePoint.X)
            {
                actorPoint = actor.connectingPointRight;
                useCasePoint = useCase.connectingPointLeft;
            }
            else if (actorPoint.X > useCasePoint.X)
            {
                actorPoint = actor.connectingPointLeft;
                useCasePoint = useCase.connectingPointRight;
            }

            lineStart = actorPoint;
            lineEnd = useCasePoint;
        }

        public Line(Point lineStart, Point lineEnd)
        {
            this.lineStart = lineStart;
            this.lineEnd = lineEnd;
            selected = false;
        }

        public void draw(Graphics graphics)
        {
            refreshConnectingPoints();

            Pen pen;

            if (selected)
                pen = new Pen(Color.Red);
            else
                pen = new Pen(actor.color);

            pen.Width = 2;
            graphics.DrawLine(pen, lineStart, lineEnd);
        }

        public bool pointInRange(Point mouse)
        {
            int startX;
            int endX;
            int startY;
            int endY;

            if (lineStart.X > lineEnd.X)
            {
                startX = lineEnd.X;
                endX = lineStart.X;
            }
            else
            {
                startX = lineStart.X;
                endX = lineEnd.X;
            }

            if (lineStart.Y > lineEnd.Y)
            {
                startY = lineEnd.Y;
                endY = lineStart.Y;
            }
            else
            {
                startY = lineStart.Y;
                endY = lineEnd.Y;
            }

            if (mouse.X >= startX && mouse.X <= endX && mouse.Y >= startY && mouse.Y <= endY)
            {
                return true;
            }
            return false;
        }

        public bool select(Point mouse)
        {
            if (pointInRange(mouse))
            {
                selected = !selected;
                return true;
            }
            return false;
        }
    }
}
