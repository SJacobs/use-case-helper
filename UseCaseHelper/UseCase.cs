﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UseCaseHelper
{
    public class UseCase
    {
        public string text;
        public Point point;
        public bool selected;
        public MainForm main;
        private PictureBox pictureBox;

        private int minX;
        private int maxX;
        private int minY;
        private int maxY;

        public Point connectingPointLeft;
        public Point connectingPointRight;

        private UseCaseProperties useCaseProperties;

        public UseCase(MainForm main, PictureBox pictureBox, string text, Point point)
        {
            this.main = main;
            this.pictureBox = pictureBox;
            this.text = text;
            this.point = point;
            selected = false;
            useCaseProperties = new UseCaseProperties(this);
        }

        public void draw(Graphics graphics)
        {
            Pen pen;

            if (selected)
                pen = new Pen(Color.Red);
            else
                pen = new Pen(Color.Black);

            pen.Width = 2;

            // Calculate dimensions.
            Font font = new Font(FontFamily.GenericSansSerif, 12);
            SizeF textSize = graphics.MeasureString(text, font);
            int x = (int)(point.X - (textSize.Width / 2) - 20);
            int y = point.Y - 30;
            int width = (int)(textSize.Width + 50);
            int height = (int)(textSize.Height + 10);

            // Draw and fill ellipse.
            bool fillEllipse = false;
            RectangleF ellipseRectangle = new RectangleF(new Point(x, y), new SizeF(width, height));
            graphics.DrawEllipse(pen, ellipseRectangle);
            if (fillEllipse)
                graphics.FillEllipse(Brushes.White, ellipseRectangle);

            // Text
            graphics.DrawString(text, new Font("Arial", 12), Brushes.Black, point.X - (textSize.Width / 2) + 4, point.Y - 24);

            // Set hitbox
            minX = (int)(point.X - (textSize.Width / 2)) - 30;
            maxX = (int)(point.X + (textSize.Width / 2)) + 30;
            minY = (int)(point.Y - textSize.Height) - 15;
            maxY = point.Y + 10;

            connectingPointLeft = new Point(point.X - (int)((textSize.Width) / 2) - 29, point.Y - (int)(textSize.Height / 2) - 5);
            connectingPointRight = new Point(point.X + (int)((textSize.Width) / 2) + 36, point.Y - (int)(textSize.Height / 2) - 5);
        }

        public bool pointInRange(Point mouse)
        {
            if (mouse.X >= minX && mouse.X <= maxX && mouse.Y >= minY && mouse.Y <= maxY)
            {
                return true;
            }
            return false;
        }

        public bool select(Point mouse)
        {
            if (pointInRange(mouse))
            {
                selected = !selected;
                return true;
            }
            return false;
        }

        public bool edit(Point mouse)
        {
            if (!pointInRange(mouse))
            {
                return false;
            }
            useCaseProperties.Show();
            return true;
        }
    }
}
