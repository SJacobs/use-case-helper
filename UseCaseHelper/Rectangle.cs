﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCaseHelper
{
    public class Rectangle
    {
        private Point start;
        public Point end;
        public bool selected;
        public bool firstClick;

        public Rectangle(Point start, Point end)
        {
            this.start = start;
            this.end = end;
            selected = false;
            firstClick = true;
        }

        public void draw(Graphics graphics)
        {
            Pen pen;

            if (selected)
                pen = new Pen(Color.Red);
            else if (firstClick)
                pen = new Pen(Color.Green);
            else
                pen = new Pen(Color.Black);

            pen.Width = 2;

            int width = end.X - start.X;
            int height = end.Y - start.Y;
            graphics.DrawRectangle(pen, start.X, start.Y, width, height);
        }
        public bool pointInRange(Point mouse)
        {
            if (mouse.X >= start.X && mouse.X <= end.X && mouse.Y >= start.Y && mouse.Y <= end.Y)
            {
                return true;
            }
            return false;
        }

        public bool select(Point mouse)
        {
            if (pointInRange(mouse))
            {
                selected = !selected;
                return true;
            }
            return false;
        }

    }
}
