﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UseCaseHelper
{
    public class Actor
    {
        private MainForm main;
        private PictureBox pictureBox;
        public int x;
        public int y;
        public string name;
        public bool selected;

        public Color color;

        public Point connectingPointLeft;
        public Point connectingPointRight;

        public Actor(MainForm main, PictureBox pictureBox, int x, int y, string name)
        {
            this.main = main;
            this.pictureBox = pictureBox;
            this.x = x;
            this.y = y;
            this.name = name;
            selected = false;
            color = getRandomColor();

            connectingPointLeft = new Point(x - 20, y + 52);
            connectingPointRight = new Point(x + 20, y + 52);
        }

        private TextBox tbNameInput;

        private Color getRandomColor()
        {
            Color[] colors = { Color.Black, Color.DarkOrange, Color.MediumTurquoise, Color.DarkRed, Color.Gray };

            main.currentActorColorIndex++;
            if (main.currentActorColorIndex >= colors.Count())
                main.currentActorColorIndex = 0;

            return colors[main.currentActorColorIndex];
        }

        private void confirmNameChange()
        {
            name = tbNameInput.Text;
            main.resetPaint();
            tbNameInput.Dispose();
        }

        public bool pointInRange(Point mouse)
        {
            if (mouse.X >= x - 30 && mouse.X <= x + 30 && mouse.Y >= y - 20 && mouse.Y <= y + 120)
            {
                return true;
            }
            return false;
        }

        public bool select(Point mouse)
        {
            if (pointInRange(mouse))
            {
                selected = !selected;
                return true;
            }
            return false;
        }

        public bool edit(Point mouse)
        {
            if (!pointInRange(mouse))
            {
                return false;
            }
            tbNameInput = new TextBox();
            tbNameInput.TextAlign = HorizontalAlignment.Center;
            tbNameInput.Width = 40 + name.Length * 12;
            tbNameInput.Location = new Point(x - (tbNameInput.Width / 2), y + 100);
            pictureBox.Controls.Add(tbNameInput);
            tbNameInput.Show();

            tbNameInput.KeyDown += TbNameInput_KeyDown;
            main.ActiveControl = tbNameInput;

            return true;
        }

        private void TbNameInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                confirmNameChange();
        }

        public void draw(Graphics graphics)
        {
            Pen pen;

            if (selected)
                pen = new Pen(Color.Red);
            else
                pen = new Pen(color);

            pen.Width = 2;

            // Actor sizes.
            int widthHead = 35;
            int heightHead = 35;
            int bodyHeight = 40;
            int legWidth = 15;
            int legHeight = 20;
            int nameOffset = 100;

            // Draw head
            graphics.DrawEllipse(pen, x - (widthHead / 2), y, widthHead, heightHead);

            bool drawSwag = false;

            if (drawSwag)
            {
                // Draw eyes
                graphics.DrawEllipse(pen, x - (widthHead / 2) + 15, y + 25, 10, 10);
                graphics.DrawEllipse(pen, x - (widthHead / 2) + 30, y + 25, 10, 10);

                // Draw mouth
                graphics.DrawLine(pen, new Point(x, y + 40), new Point(x + 20, y + 40));

                // Draw hat 
                graphics.DrawLine(pen, new Point(x - 24, y + 25), new Point(x + 50, y + 25));
            }

            // Draw body
            Point bodyStart = new Point(x, y + heightHead);
            Point bodyEnd = new Point(x, y + heightHead + bodyHeight);
            graphics.DrawLine(pen, bodyStart, bodyEnd);

            // Draw arm
            Point armStart = new Point(x - 15, (bodyEnd.Y + bodyStart.Y) / 2);
            Point armEnd = new Point(x + 15, (bodyEnd.Y + bodyStart.Y) / 2);
            graphics.DrawLine(pen, armStart, armEnd);

            // Left leg
            Point leftLegStart = new Point(x, bodyEnd.Y);
            Point leftLegEnd = new Point(x - legWidth, bodyEnd.Y + legHeight);
            graphics.DrawLine(pen, leftLegStart, leftLegEnd);

            // Right leg
            Point rightLegStart = new Point(x, bodyEnd.Y);
            Point rightLegEnd = new Point(x + legWidth, bodyEnd.Y + legHeight);
            graphics.DrawLine(pen, rightLegStart, rightLegEnd);

            // Actor name
            Font font = new Font("Arial", 12);
            SizeF textSize = graphics.MeasureString(name, font);
            graphics.DrawString(name, font, Brushes.Black, x - (textSize.Width / 2), y + nameOffset);
        }
    }
}
