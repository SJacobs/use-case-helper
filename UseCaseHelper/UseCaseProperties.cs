﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UseCaseHelper
{
    public partial class UseCaseProperties : Form
    {
        private UseCase useCase;

        private string summary;
        private string actors;
        private string assumptions;
        private string description;
        private string exceptions;
        private string result;

        public UseCaseProperties(UseCase useCase)
        {
            InitializeComponent();
            this.useCase = useCase;
            tbName.Text = useCase.text;
        }

        private void UseCaseProperties_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            Parent = null;
            e.Cancel = true;

            useCase.text = tbName.Text;
            useCase.main.resetPaint();

            summary = tbSummary.Text;
            actors = tbActors.Text;
            assumptions = tbAssumptions.Text;
            description = rtbDescription.Text;
            exceptions = rtbExceptions.Text;
            result = tbResult.Text;
        }
    }
}
