﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UseCaseHelper
{
    public partial class MainForm : Form
    {
        private List<Actor> actors = new List<Actor>();
        private List<UseCase> useCases = new List<UseCase>();
        private List<Line> lines = new List<Line>();
        private List<Rectangle> rectangles = new List<Rectangle>();
        public int currentActorColorIndex = -1;

        public MainForm()
        {
            InitializeComponent();
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            foreach (Actor actor in actors)
            {
                actor.draw(e.Graphics);
            }
            foreach (UseCase useCase in useCases)
            {
                useCase.draw(e.Graphics);
            }
            foreach (Line line in lines)
            {
                line.draw(e.Graphics);
            }
            foreach (Rectangle rectangle in rectangles)
            {
                rectangle.draw(e.Graphics);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (Actor actor in actors.ToArray())
            {
                if (actor.selected)
                {
                    actors.Remove(actor);
                }
            }

            foreach (UseCase useCase in useCases.ToArray())
            {
                if (useCase.selected)
                {
                    useCases.Remove(useCase);
                }
            }

            foreach (Line line in lines.ToArray())
            {
                if (line.selected)
                {
                    lines.Remove(line);
                }
            }

            foreach (Rectangle rectangle in rectangles.ToArray())
            {
                if (rectangle.selected)
                {
                    rectangles.Remove(rectangle);
                }
            }

            resetPaint();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            actors.Clear();
            useCases.Clear();
            lines.Clear();
            rectangles.Clear();
            resetPaint();
        }

        private void handleCreate(int mouseX, int mouseY)
        {
            if (radioActor.Checked)
                actors.Add(new Actor(this, pictureBox, mouseX, mouseY - 70, "User"));
            else if (radioUseCase.Checked)
                useCases.Add(new UseCase(this, pictureBox, "UseCase", new Point(mouseX, mouseY + 15)));
            else if (radioRectangle.Checked)
                createRectangle(mouseX, mouseY);
        }

        private void createRectangle(int mouseX, int mouseY)
        {
            if (rectangles.Count >= 1)
            {
                Rectangle rectangle = rectangles[rectangles.Count - 1];
                if (rectangle.firstClick)
                {
                    rectangle.end = new Point(mouseX, mouseY);
                    rectangle.firstClick = false;
                }

                else
                {
                    Rectangle rectangleNew = new Rectangle(new Point(mouseX, mouseY), new Point(mouseX + 50, mouseY + 50));
                    rectangles.Add(rectangleNew);
                }

            }
            else {
                Rectangle rectangleNew = new Rectangle(new Point(mouseX, mouseY), new Point(mouseX + 50, mouseY + 50));
                rectangles.Add(rectangleNew);
            }
        }

        private void handleSelect(int mouseX, int mouseY)
        {
            foreach (Actor actor in actors)
            {
                if (actor.select(new Point(mouseX, mouseY)))
                {
                    return;
                }
            }

            foreach (UseCase useCase in useCases)
            {
                if (useCase.select(new Point(mouseX, mouseY)))
                {
                    return;
                }
            }

            foreach (Line line in lines)
            {
                if (line.select(new Point(mouseX, mouseY)))
                {
                    return;
                }
            }

            foreach (Rectangle rectangle in rectangles)
            {
                if (rectangle.select(new Point(mouseX, mouseY)))
                {
                    return;
                }
            }
        }

        private void handleEdit(int mouseX, int mouseY)
        {
            foreach (Actor actor in actors)
            {
                if (actor.edit(new Point(mouseX, mouseY)))
                {
                    return;
                }
            }
            foreach (UseCase useCase in useCases)
            {
                if (useCase.edit(new Point(mouseX, mouseY)))
                {
                    return;
                }
            }
        }

        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (radioCreate.Checked)
            {
                handleCreate(e.X, e.Y);
            }
            else if (radioSelect.Checked)
            {
                handleSelect(e.X, e.Y);
            }
            else if (radioEdit.Checked)
            {
                handleEdit(e.X, e.Y);
            }
            resetPaint();
        }

        public void resetPaint()
        {
            pictureBox.Invalidate();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            foreach (Actor actor in actors)
            {
                if (actor.selected)
                {
                    foreach (UseCase useCase in useCases)
                    {
                        if (useCase.selected)
                        {
                            lines.Add(new Line(actor, useCase));
                        }
                    }
                }
            }
            resetSelects();
            resetPaint();
        }

        private void resetSelects()
        {
            foreach (Actor actor in actors)
            {
                actor.selected = false;
            }

            foreach (UseCase useCase in useCases)
            {
                useCase.selected = false;
            }

            foreach (Line line in lines)
            {
                line.selected = false;
            }

            foreach (Rectangle rectangle in rectangles)
            {
                rectangle.selected = false;
            }
        }

        private void radioSelect_CheckedChanged(object sender, EventArgs e)
        {
            if (radioSelect.Checked)
                btnConnect.Enabled = true;
            else
                btnConnect.Enabled = false;
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (radioActor.Checked)
            {
                if (actors.Count > 0)
                    actors.RemoveAt(actors.Count - 1);
            }
            else if (radioUseCase.Checked)
            {
                if (useCases.Count > 0)
                    useCases.RemoveAt(useCases.Count - 1);
            }
            else if (radioLine.Checked)
            {
                if (lines.Count > 0)
                    lines.RemoveAt(lines.Count - 1);
            }
            else if (radioRectangle.Checked)
            {
                if (rectangles.Count > 0)
                    rectangles.RemoveAt(rectangles.Count - 1);
            }
            resetPaint();
        }
    }
}
