﻿namespace UseCaseHelper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupElements = new System.Windows.Forms.GroupBox();
            this.radioRectangle = new System.Windows.Forms.RadioButton();
            this.radioLine = new System.Windows.Forms.RadioButton();
            this.radioUseCase = new System.Windows.Forms.RadioButton();
            this.radioActor = new System.Windows.Forms.RadioButton();
            this.groupModes = new System.Windows.Forms.GroupBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.radioEdit = new System.Windows.Forms.RadioButton();
            this.radioSelect = new System.Windows.Forms.RadioButton();
            this.radioCreate = new System.Windows.Forms.RadioButton();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.btnUndo = new System.Windows.Forms.Button();
            this.groupElements.SuspendLayout();
            this.groupModes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupElements
            // 
            this.groupElements.Controls.Add(this.radioRectangle);
            this.groupElements.Controls.Add(this.radioLine);
            this.groupElements.Controls.Add(this.radioUseCase);
            this.groupElements.Controls.Add(this.radioActor);
            this.groupElements.Location = new System.Drawing.Point(12, 12);
            this.groupElements.Name = "groupElements";
            this.groupElements.Size = new System.Drawing.Size(182, 122);
            this.groupElements.TabIndex = 0;
            this.groupElements.TabStop = false;
            this.groupElements.Text = "Elements";
            // 
            // radioRectangle
            // 
            this.radioRectangle.AutoSize = true;
            this.radioRectangle.Location = new System.Drawing.Point(6, 65);
            this.radioRectangle.Name = "radioRectangle";
            this.radioRectangle.Size = new System.Drawing.Size(74, 17);
            this.radioRectangle.TabIndex = 3;
            this.radioRectangle.TabStop = true;
            this.radioRectangle.Text = "Rectangle";
            this.radioRectangle.UseVisualStyleBackColor = true;
            // 
            // radioLine
            // 
            this.radioLine.AutoSize = true;
            this.radioLine.Location = new System.Drawing.Point(6, 88);
            this.radioLine.Name = "radioLine";
            this.radioLine.Size = new System.Drawing.Size(45, 17);
            this.radioLine.TabIndex = 2;
            this.radioLine.TabStop = true;
            this.radioLine.Text = "Line";
            this.radioLine.UseVisualStyleBackColor = true;
            // 
            // radioUseCase
            // 
            this.radioUseCase.AutoSize = true;
            this.radioUseCase.Location = new System.Drawing.Point(6, 42);
            this.radioUseCase.Name = "radioUseCase";
            this.radioUseCase.Size = new System.Drawing.Size(70, 17);
            this.radioUseCase.TabIndex = 1;
            this.radioUseCase.TabStop = true;
            this.radioUseCase.Text = "Use case";
            this.radioUseCase.UseVisualStyleBackColor = true;
            // 
            // radioActor
            // 
            this.radioActor.AutoSize = true;
            this.radioActor.Checked = true;
            this.radioActor.Location = new System.Drawing.Point(6, 19);
            this.radioActor.Name = "radioActor";
            this.radioActor.Size = new System.Drawing.Size(50, 17);
            this.radioActor.TabIndex = 0;
            this.radioActor.TabStop = true;
            this.radioActor.Text = "Actor";
            this.radioActor.UseVisualStyleBackColor = true;
            // 
            // groupModes
            // 
            this.groupModes.Controls.Add(this.btnConnect);
            this.groupModes.Controls.Add(this.radioEdit);
            this.groupModes.Controls.Add(this.radioSelect);
            this.groupModes.Controls.Add(this.radioCreate);
            this.groupModes.Location = new System.Drawing.Point(200, 12);
            this.groupModes.Name = "groupModes";
            this.groupModes.Size = new System.Drawing.Size(209, 122);
            this.groupModes.TabIndex = 1;
            this.groupModes.TabStop = false;
            this.groupModes.Text = "Modes";
            // 
            // btnConnect
            // 
            this.btnConnect.Enabled = false;
            this.btnConnect.Location = new System.Drawing.Point(128, 93);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 4;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // radioEdit
            // 
            this.radioEdit.AutoSize = true;
            this.radioEdit.Location = new System.Drawing.Point(6, 65);
            this.radioEdit.Name = "radioEdit";
            this.radioEdit.Size = new System.Drawing.Size(43, 17);
            this.radioEdit.TabIndex = 2;
            this.radioEdit.Text = "Edit";
            this.radioEdit.UseVisualStyleBackColor = true;
            // 
            // radioSelect
            // 
            this.radioSelect.AutoSize = true;
            this.radioSelect.Location = new System.Drawing.Point(6, 42);
            this.radioSelect.Name = "radioSelect";
            this.radioSelect.Size = new System.Drawing.Size(55, 17);
            this.radioSelect.TabIndex = 1;
            this.radioSelect.Text = "Select";
            this.radioSelect.UseVisualStyleBackColor = true;
            this.radioSelect.CheckedChanged += new System.EventHandler(this.radioSelect_CheckedChanged);
            // 
            // radioCreate
            // 
            this.radioCreate.AutoSize = true;
            this.radioCreate.Checked = true;
            this.radioCreate.Location = new System.Drawing.Point(6, 19);
            this.radioCreate.Name = "radioCreate";
            this.radioCreate.Size = new System.Drawing.Size(56, 17);
            this.radioCreate.TabIndex = 0;
            this.radioCreate.TabStop = true;
            this.radioCreate.Text = "Create";
            this.radioCreate.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(1289, 96);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(135, 36);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear all";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(1289, 12);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(135, 36);
            this.btnRemove.TabIndex = 3;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Location = new System.Drawing.Point(12, 140);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(1412, 643);
            this.pictureBox.TabIndex = 4;
            this.pictureBox.TabStop = false;
            this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Paint);
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseClick);
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(1289, 54);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(135, 36);
            this.btnUndo.TabIndex = 5;
            this.btnUndo.Text = "Remove last";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1436, 795);
            this.Controls.Add(this.btnUndo);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.groupModes);
            this.Controls.Add(this.groupElements);
            this.Name = "MainForm";
            this.Text = "Use case helper (Simin Jacobs)";
            this.groupElements.ResumeLayout(false);
            this.groupElements.PerformLayout();
            this.groupModes.ResumeLayout(false);
            this.groupModes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupElements;
        private System.Windows.Forms.RadioButton radioUseCase;
        private System.Windows.Forms.RadioButton radioActor;
        private System.Windows.Forms.GroupBox groupModes;
        private System.Windows.Forms.RadioButton radioCreate;
        private System.Windows.Forms.RadioButton radioSelect;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.RadioButton radioEdit;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.RadioButton radioLine;
        private System.Windows.Forms.RadioButton radioRectangle;
        private System.Windows.Forms.Button btnUndo;
    }
}

