﻿namespace UseCaseHelper
{
    partial class UseCaseProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbSummary = new System.Windows.Forms.TextBox();
            this.tbActors = new System.Windows.Forms.TextBox();
            this.tbAssumptions = new System.Windows.Forms.TextBox();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.rtbExceptions = new System.Windows.Forms.RichTextBox();
            this.lbSummary = new System.Windows.Forms.Label();
            this.lbActors = new System.Windows.Forms.Label();
            this.lbAssumptions = new System.Windows.Forms.Label();
            this.lbDescription = new System.Windows.Forms.Label();
            this.lbExceptions = new System.Windows.Forms.Label();
            this.lbResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(12, 15);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(38, 13);
            this.lbName.TabIndex = 0;
            this.lbName.Text = "Name:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(129, 12);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(243, 20);
            this.tbName.TabIndex = 1;
            // 
            // tbSummary
            // 
            this.tbSummary.Location = new System.Drawing.Point(129, 38);
            this.tbSummary.Name = "tbSummary";
            this.tbSummary.Size = new System.Drawing.Size(243, 20);
            this.tbSummary.TabIndex = 2;
            // 
            // tbActors
            // 
            this.tbActors.Location = new System.Drawing.Point(129, 64);
            this.tbActors.Name = "tbActors";
            this.tbActors.Size = new System.Drawing.Size(243, 20);
            this.tbActors.TabIndex = 3;
            // 
            // tbAssumptions
            // 
            this.tbAssumptions.Location = new System.Drawing.Point(129, 90);
            this.tbAssumptions.Name = "tbAssumptions";
            this.tbAssumptions.Size = new System.Drawing.Size(243, 20);
            this.tbAssumptions.TabIndex = 4;
            // 
            // tbResult
            // 
            this.tbResult.Location = new System.Drawing.Point(129, 354);
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(243, 20);
            this.tbResult.TabIndex = 7;
            // 
            // rtbDescription
            // 
            this.rtbDescription.Location = new System.Drawing.Point(129, 116);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(243, 113);
            this.rtbDescription.TabIndex = 5;
            this.rtbDescription.Text = "";
            // 
            // rtbExceptions
            // 
            this.rtbExceptions.Location = new System.Drawing.Point(129, 235);
            this.rtbExceptions.Name = "rtbExceptions";
            this.rtbExceptions.Size = new System.Drawing.Size(243, 113);
            this.rtbExceptions.TabIndex = 6;
            this.rtbExceptions.Text = "";
            // 
            // lbSummary
            // 
            this.lbSummary.AutoSize = true;
            this.lbSummary.Location = new System.Drawing.Point(12, 41);
            this.lbSummary.Name = "lbSummary";
            this.lbSummary.Size = new System.Drawing.Size(53, 13);
            this.lbSummary.TabIndex = 8;
            this.lbSummary.Text = "Summary:";
            // 
            // lbActors
            // 
            this.lbActors.AutoSize = true;
            this.lbActors.Location = new System.Drawing.Point(12, 67);
            this.lbActors.Name = "lbActors";
            this.lbActors.Size = new System.Drawing.Size(37, 13);
            this.lbActors.TabIndex = 9;
            this.lbActors.Text = "Actors";
            // 
            // lbAssumptions
            // 
            this.lbAssumptions.AutoSize = true;
            this.lbAssumptions.Location = new System.Drawing.Point(12, 93);
            this.lbAssumptions.Name = "lbAssumptions";
            this.lbAssumptions.Size = new System.Drawing.Size(69, 13);
            this.lbAssumptions.TabIndex = 10;
            this.lbAssumptions.Text = "Assumptions:";
            // 
            // lbDescription
            // 
            this.lbDescription.AutoSize = true;
            this.lbDescription.Location = new System.Drawing.Point(12, 119);
            this.lbDescription.Name = "lbDescription";
            this.lbDescription.Size = new System.Drawing.Size(63, 13);
            this.lbDescription.TabIndex = 11;
            this.lbDescription.Text = "Description:";
            // 
            // lbExceptions
            // 
            this.lbExceptions.AutoSize = true;
            this.lbExceptions.Location = new System.Drawing.Point(12, 238);
            this.lbExceptions.Name = "lbExceptions";
            this.lbExceptions.Size = new System.Drawing.Size(59, 13);
            this.lbExceptions.TabIndex = 12;
            this.lbExceptions.Text = "Exceptions";
            // 
            // lbResult
            // 
            this.lbResult.AutoSize = true;
            this.lbResult.Location = new System.Drawing.Point(12, 357);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(40, 13);
            this.lbResult.TabIndex = 13;
            this.lbResult.Text = "Result:";
            // 
            // UseCaseProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 385);
            this.Controls.Add(this.lbResult);
            this.Controls.Add(this.lbExceptions);
            this.Controls.Add(this.lbDescription);
            this.Controls.Add(this.lbAssumptions);
            this.Controls.Add(this.lbActors);
            this.Controls.Add(this.lbSummary);
            this.Controls.Add(this.rtbExceptions);
            this.Controls.Add(this.rtbDescription);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.tbAssumptions);
            this.Controls.Add(this.tbActors);
            this.Controls.Add(this.tbSummary);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lbName);
            this.Name = "UseCaseProperties";
            this.Text = "Properties (Simin Jacobs)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UseCaseProperties_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbAssumptions;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.RichTextBox rtbExceptions;
        private System.Windows.Forms.Label lbAssumptions;
        private System.Windows.Forms.Label lbExceptions;
        private System.Windows.Forms.Label lbResult;
        private System.Windows.Forms.Label lbDescription;
        private System.Windows.Forms.Label lbActors;
        private System.Windows.Forms.Label lbSummary;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.TextBox tbActors;
        private System.Windows.Forms.TextBox tbSummary;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lbName;
    }
}